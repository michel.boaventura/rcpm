lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'rcpm/version'

Gem::Specification.new do |spec|
  spec.name          = "rcpm"
  spec.version       = RCPM::VERSION
  spec.authors       = ["Michel Boaventura"]
  spec.email         = ["michel.boaventura@gmail.com"]

  spec.summary       = %q{Ruby's implementation of Clique Percolation Method}
  spec.homepage      = "http://github.com/michelboaventura/rcpm"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.executables   = ["rcpm"]
  spec.require_paths = ["lib"]

  spec.extensions = %w[ext/bk/extconf.rb]

  spec.add_development_dependency "bundler", "~> 1.8"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rake-compiler", "~> 0.9"
end
