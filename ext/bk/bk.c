#include <ruby.h>

void print_array(VALUE arr) {
  printf("%s\n",RSTRING_PTR(rb_inspect(arr)));
}

void bkv2(VALUE m, VALUE old_set, int ne, int ce, VALUE cliques, VALUE potential_clique) {
  VALUE new_set;

  int nod, fixp, newne, newce, i, j, count, pos, p, s, sel, minnod;

  new_set = rb_ary_new2(ce);
  minnod = ce;
  nod = 0;
  fixp = 0;
  pos = 0;
  s = 0;

  /* Determine each counter value and look for minimum */
  for (i = 0 ; i < ce && minnod != 0; i++) {
    p = NUM2INT(rb_ary_entry(old_set, i));
    count = 0;

    /* Count disconnections */
    for (j = ne; j < ce && count < minnod; j++) {
      if(!NUM2INT(rb_ary_entry(rb_ary_entry(m, p), NUM2INT(rb_ary_entry(old_set, j))))) {
        count++;
        /* Save position of potential candidate */
        pos = j;
      }
    }

    /* Test new minimum */
    if (count < minnod) {
      fixp = p;
      minnod = count;

      if (i < ne) {
        s = pos;
      }
      else {
        s = i;
        /* pre-increment */
        nod = 1;
      }
    }
  }

  /* If fixed point initially chosen from candidates then
     number of diconnections will be preincreased by one */

  /* Backtrackcycle */
  for (nod=minnod+nod; nod>=1; nod--) {
    /* Interchange */
    p = NUM2INT(rb_ary_entry(old_set, s));
    rb_ary_store(old_set, s, rb_ary_entry(old_set, ne));
    sel = p;
    rb_ary_store(old_set, ne, INT2NUM(sel));

    /* Fill new set "not" */
    newne = 0;
    for (i = 0 ; i < ne ; i++) {
      if(NUM2INT(rb_ary_entry(rb_ary_entry(m, sel), NUM2INT(rb_ary_entry(old_set, i))))) {
        rb_ary_store(new_set, newne++, rb_ary_entry(old_set, i));
      }
    }

    /*Fill new set "cand" */
    newce = newne;
    for (i=ne+1; i < ce; i++) {
      if(NUM2INT(rb_ary_entry(rb_ary_entry(m, sel), NUM2INT(rb_ary_entry(old_set, i))))) {
        rb_ary_store(new_set, newce++, rb_ary_entry(old_set, i));
      }
    }

    rb_ary_push(potential_clique, INT2NUM(sel));

    if (newce == 0) {
      rb_ary_push(cliques, rb_ary_dup(potential_clique));
    }
    else if (newne < newce) {
      bkv2(m, new_set, newne, newce, cliques, potential_clique);
    }

    rb_ary_pop(potential_clique);

    /* Add to "not" */
    ne++;
    if (nod > 1) {
      /* Select a candidate disconnected to the fixed point */

      for(s = ne; NUM2INT((rb_ary_entry(rb_ary_entry(m, fixp), NUM2INT(rb_ary_entry(old_set, s))))); s++);
    }

  } /* Backtrackcycle */
}

static VALUE bk(VALUE arr) {
  int i, n;
  VALUE all, cliques, potential_clique;
  
  n = RARRAY_LEN(arr);

  all = rb_ary_new2(n);
  potential_clique = rb_ary_new();
  cliques = rb_ary_new();
  
  for (i = 0; i < n; i++) {
    rb_ary_push(all, INT2NUM(i));
  }

  bkv2(arr, all, 0, n, cliques, potential_clique);

  return cliques;
}

void Init_bk() {
  rb_define_method(rb_cArray, "bk", bk, 0);
}
